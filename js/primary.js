$(document).ready(function(){
    var button=$('#button');
    var camera=$('#camera');
    var light_detecter=$('#light_detecter');
    button.width(button.height());
    camera.width(camera.height());
    light_detecter.width(light_detecter.height());
    $('.app .icon').click(function(){
        var id=$(this).attr('id').replace("site","");
        $.getJSON('./sites.json',function(data){
            var result=data[id];
            $('#modal-text').html("<div id='left'>Nom : "+result['name']+"<br />Status : "+result['status']+"<br /><a href='"+result['link']+"' class='link-button material-indigo'>Voir le site</a><br /></div><div id='right'>Les languages utilisé : <br /></div>");
            if(result['html']==true) $('#right').append('<i class="fab fa-html5"></i><br /> ');
            if(result['css']==true) $('#right').append('<i class="fab fa-css3-alt"></i><br /> ');
            if(result['bootstrap']==true) $('#right').append('Bootstrap<br /> ');
            if(result['angular']==true) $('#right').append('<i class="fab fa-angular"></i><br /> ');
            if(result['laravel']==true) $('#right').append('<i class="fab fa-laravel"></i><br /> ');
            if(result['java']==true) $('#right').append('<i class="fab fa-java"></i><br /> ');
            if(result['php']==true) $('#right').append('<i class="fab fa-php"></i><br /> ');
            if(result['mysql']==true) $('#right').append('MySQL<br /> ');
            if(result['symfony']==true) $('#right').append('Symfony<br /> ');
            if(result['nodejs']==true) $('#right').append('<i class="fab fa-node"></i><br /> ');
            if(result['javascript']==true) $('#right').append('<i class="fab fa-js"></i><br /> ');
            if(result['typescript']==true) $('#right').append('TypeScript<br /> ');
            if(result['spring']==true) $('#right').append('Spring<br /> ');
            if(result['vuejs']==true) $('#right').append('<i class="fab fa-vuejs"></i><br /> ');
        });
        $('#myModal').fadeIn(200).css('display','flex');
    })
    $('#contacts').click(function(){
        $('#modal-text').html("<section><i class='fas fa-phone'></i><br />0789661496</section><section><i class='fas fa-envelope'></i><br />kartushin.alexander97@gmail.com</section>");
        $('#myModal').fadeIn(200).css('display','flex');
    })
    $('#info').click(function(){
        $('#modal-text').html("<span><h1>KARTUSHIN Alexander</h1><br />Dévéloppeur WEB<br />20 Ans<br />Autodidacte,Créatif et trés Curieux<br />Chaque jour j'apprends des nouveaux languages de programmation/frameworks, j'ameliore mes projets existants, passionné de web et des technologies </span>");
        $('#myModal').fadeIn(200).css('display','flex');
    })
    $('.close').click(function(){
        $('#myModal').fadeOut(200);
    })
})