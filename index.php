<!doctype html>
<html lang="fr">
    <head>
<!-- git clone https://Ushinnary@bitbucket.org/Ushinnary/cvdevice.git -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kartushin Alexander</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/primary.css">
    </head>
    <body>
		<div id="main">
				<!-- Modal -->
				<div id="myModal" class="modal">
					<div class="top"></div>
					<div class="modal-content">
						<!-- <span class="close">&times;</span> -->
						<p id="modal-text"></p>
					</div>
					<div id="illusion"></div>
				</div>
	<!-- Border -->
            <div id="outside">
			<!-- Top elements -->
                <div class="top">
                    <span id="camera"></span>
                    <span id="speaker"></span>
                    <span id="light_detecter"></span>
                </div>
				<!-- Screen area -->
                <div id="inside">
									<div id="all">
										<?php 
											$str = file_get_contents('sites.json');
											$json=json_decode($str,true);
											// $json=array_reverse($json);
											for($i=0;$i<sizeof($json);$i++){
												$random=rand(1,6);
												switch($random){
													case(1): $style='material-red';break;
													case(2): $style='material-purple';break;
													case(3): $style='material-indigo';break;
													case(4): $style='material-blue';break;
													case(5): $style='material-green';break;
													case(6): $style='material-yellow';break;
												}
												$letter_first = strtoupper(substr($json[$i]['name'], 0,1));
												$letter_last = strtoupper(substr($json[$i]['name'], -1));
												echo "<div class='app'><div class='icon $style' id='site$i'>".$letter_first.$letter_last."</div>".$json[$i]['name']."</div>";
											}
											?>
											</div>
										<div id="bottom">
											<section>
											<div class="icon material-blue"><a href="tel:+33789661496"><i class="fas fa-phone"></i></a></div>
											Téléphoner
											</section>
											<section>
											<div class="icon material-blue"><a href="mailto:kartushin.alexander97@gmail.com"><i class="fas fa-envelope"></i></a></div>Envoyer Mail
											</section>
											<section>
												<div class="icon material-blue" id="contacts"><i class="fas fa-user"></i></div>Contacts
											</section>
											<section>
												<div class="icon material-blue" id="info"><i class="fas fa-info"></i></div>A propos de moi
											</section>
										</div>
                </div>
				<!-- Home Button -->
                <div id="button" class="close"></div>
            </div>
        </div>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/primary.js"></script>
</html>
